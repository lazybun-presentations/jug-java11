## How many changes were there?

---

### 946 classes changed

Note:
Source - https://gunnarmorling.github.io/jdk-api-diff/jdk10-jdk11-api-diff.html

---

### New: 93

#### `java.net.http`
#### `jdk.jfr`

 <!-- * java.net.http (JEP 321: HTTP Client (Standard))
 * some stuff in java.security
 * jdk.jfr (Flight Recorder included in Java11)
 * some other misc packages
 -->
---

### Removed: 787

#### `javax`
#### `org.omg`
 <!-- * lots of javax (JEP 320: Remove The Java EE and CORBA Modules)
 * org.omg (JEP 320: Remove The Java EE and CORBA Modules)
 * jdk.incubator.http (JEP 321: HTTP Client (Standard)) 
  * Moved to java.net.http
 * sun.reflect.Reflection -->

Note:
CORBA!! nie Cobra

---

### Modified: 66