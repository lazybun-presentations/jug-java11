
##### java.lang.Character

 `String toString(int):` 
 

 `This is an overloaded form of the existing method but takes an int instead of a char. The int is a Unicode code point.`

+++

```java
//Pre-Java 11
Character.toChars(0420); //char[]

//Java 11          \/ Unicode code point
Character.toString(0420);
```

Note:
Character.toString - czym jest ten int konkretnie? -- generalnie int ale można spokojnie podać octal integer