##### java.lang.CharSequence

`int compare(CharSequence, CharSequence):` 


`Compares two CharSequence instances lexicographically.`

+++

```java
String s1 = "jug";
StringBuilder builder = new StringBuilder();
builder.append("jug");

//Pre-Java 11
s1.compareTo(builder.toString());

//Java 11
CharSequence.compare(s1, builder);
```
