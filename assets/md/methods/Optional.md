##### java.util.(Optional & OptionalInt & OptionalDouble & OptionalLong)

 `boolean isEmpty()` 


 `If a value is not present, it returns true, otherwise it is false.`

+++

```java
var optional = Optional.of("");

//Pre-Java 11
!optional.isPresent();
//Java 11
optional.isEmpty();
```
