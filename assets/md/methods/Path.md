##### java.nio.file.Path

 `Path of(String, String[]): Returns a Path by converting a path string, or a sequence of strings that when joined form a path string.`

 
 `Path of(net.URI): Returns a Path by converting a URI.`

+++

```java
//Pre-Java 11
Paths.get("path", "to", "file");
Paths.get(URI.create("uri"));

//Java 11
Path.of("path", "to", "file");
Path.of(URI.create("uri"));
//`Paths.get`s get redirected to those now
```
