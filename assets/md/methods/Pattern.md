##### java.util.regex.Pattern

 `Predicate asMatchPredicate()` 


 `Creates a predicate that tests if this pattern matches a given input string.`

+++

```java
Pattern regex = Pattern.compile("[a-z]+");

//Pre-Java 11
//Creates a predicate that tests if
//this pattern matches given input string OR ANY SUBSTRING
//(uses find() underneath)
Predicate<String> substringPredicate = regex.asPredicate();

assert substringPredicate.test("9jug2");
```

+++

```java
Pattern regex = Pattern.compile("[a-z]+");

//Java 11
//Creates a predicate that tests if 
//this pattern matches ENTIRE given input string
//(uses matches() underneath)
Predicate<String> stringPredicate = regex.asMatchPredicate()

assert not(stringPredicate).test("9jug2");
assert stringPredicate.test("jug");
```