##### java.nio.file.Files

<ul style="font-size: 1.5rem;">
<li><code>String readString(Path): Reads all content from a file (UTF-8)</code></li>
<li><code>String readString(Path, Charset): As above, except specified Charset</code></li>
<li><code>Path writeString(Path, CharSequence, OpenOption[]): Write a CharSequence to a file (UTF-8)</code></li>
<li><code>Path writeString(Path, CharSequence, Charset, OpenOption[]): As above, except specified charset.</code></li>
</ul>

+++

```java

//Pre-Java 11
//UTF-8
Files.readAllLines(Paths.get("path")); //List<String>
//Custom Charset
Files.readAllLines(Paths.get("path"), 
				   Charset.defaultCharset());

//Writes using UTF-8
Files.write(Paths.get("path"), List.of("text"), WRITE);
//Custom Charset
Files.write(Paths.get("path"), List.of("text"), 
	        Charset.defaultCharset(), WRITE);

```

+++

```java
//Java 11

//UTF-8
Files.readString(Paths.get("path")); //String
//Custom Charset
Files.readString(Paths.get("path"), 
				 Charset.defaultCharset());

//Writes using UTF-8
Files.writeString(Paths.get("path"), "text", WRITE);
//Custom Charset
Files.writeString(Paths.get("path"), "text", 
	              Charset.defaultCharset(), WRITE);
```
