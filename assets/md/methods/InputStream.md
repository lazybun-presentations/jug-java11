##### java.io.(InputStream & OutputStream & Reader & Writer)

 * `io.InputStream nullInputStream(): Returns an InputStream that reads no bytes.`
 * `io.OutputStream nullOutputStream()`
 * `io.Reader nullReader()`
 * `io.Writer nullWriter()`

+++

##### Pre
```java
new InputStream() {
    private volatile boolean closed;
    private void ensureOpen() throws IOException {
        if (closed) {
            throw new IOException("Stream closed");
        }
    }
    @Override
    public int available () throws IOException {
        ensureOpen();
        return 0;
    }
    @Override
    public int read() throws IOException {
        ensureOpen();
        return -1;
    }
    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        Objects.checkFromIndexSize(off, len, b.length);
        if (len == 0) {
            return 0;
        }
        ensureOpen();
        return -1;
    }
    @Override
    public byte[] readAllBytes() throws IOException {
        ensureOpen();
        return new byte[0];
    }
    @Override
    public int readNBytes(byte[] b, int off, int len)
            throws IOException {
        Objects.checkFromIndexSize(off, len, b.length);
        ensureOpen();
        return 0;
    }
    @Override
    public byte[] readNBytes(int len) throws IOException {
        if (len < 0) {
            throw new IllegalArgumentException("len < 0");
        }
        ensureOpen();
        return new byte[0];
    }
    @Override
    public long skip(long n) throws IOException {
        ensureOpen();
        return 0L;
    }
    @Override
    public long transferTo(OutputStream out) throws IOException {
        Objects.requireNonNull(out);
        ensureOpen();
        return 0L;
    }
    @Override
    public void close() throws IOException { closed = true; }
};
```

Note:
You had to do this thing called do-it-yourself

+++

##### Java 11

```java
InputStream.nullInputStream();
OutputStream.nullOutputStream();

Reader.nullReader();
Writer.nullWriter();
```

Note:
But now they just gave us their implementation

TODO: Why tho
https://bugs.openjdk.java.net/browse/JDK-4358774
