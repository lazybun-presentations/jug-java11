##### java.util.concurrent.PriorityBlockingQueue
##### java.util.PriorityQueue
<ul style="font-size: 1.5rem;">
	<li><code>void forEach(Consumer)</code></li>
	<li><code>boolean removeAll(Collection)</code></li>
	<li><code>boolean removeIf(Predicate)</code></li>
	<li><code>boolean retainAll(Collection)</code>
	</li>
</ul>

+++

```java
var priorityBlockingQueue = new PriorityBlockingQueue();
var priorityQueue = new PriorityQueue();

//For each execute function
priorityBlockingQueue.forEach(item -> {});

//Remove from queue items that are 
//in both queue and given collection
priorityQueue.removeAll(
	Collections.singletonList("stuff")
);
```

+++

```java
//Remove item from queue if given predicate is met
priorityBlockingQueue.removeIf(
	item -> item.equals("stuff")
);

//Retain all queue items that are in given collection
priorityQueue.retainAll(Collections.singletonList("stuff"));
```
