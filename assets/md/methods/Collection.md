##### java.util.Collection

 `Object[] toArray(IntFunction):` 


 `Returns an array containing all of the elements in this collection, using the provided generator function to allocate the returned array.`

+++

```java
//Pre-Java 11
List<String> list = Collections.singletonList("");
//Loses type
Object[] objects = list.toArray();
//Keeps type, but long
String[] strings = list.toArray(new String[0]);

//Java 11
String[] strings = list.toArray(String[]::new);
```


Note:
IntFunction, because it essentially passes 0 as a parameter to creator method, as we can see in second example
Why 0 and not length? Possibly because computing length can be expensive for some collections
