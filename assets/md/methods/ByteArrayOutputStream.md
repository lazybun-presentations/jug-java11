##### java.io.ByteArrayOutputStream

`void writeBytes(byte[]): Write all the bytes of the parameter to the output stream`

+++

```java
var byteArray = new byte[] {};
var byteArrayOutputStream = new ByteArrayOutputStream();

//Pre-Java 11
byteArrayOutputStream.write(byteArray, 0, byteArray.length);

//Java11
byteArrayOutputStream.writeBytes(byteArray);
```
