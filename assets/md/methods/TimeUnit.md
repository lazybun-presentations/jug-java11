##### java.util.concurrent.TimeUnit

 `long convert(java.time.Duration): Converts the given time duration to this unit.`

+++

```java
//Pre-Java 11
TimeUnit.DAYS.convert(24, TimeUnit.HOURS);

//Java 11
TimeUnit.DAYS.convert(Duration.ofHours(24));
```
