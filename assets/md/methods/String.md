#### java.lang.String

+++

`boolean isBlank()` 


`Returns true if the string is empty or contains only white space`

+++

```java
//Pre-Java 11
boolean isBlank = foobar.trim().length() == 0
//Java 11
foobar.isBlank(); 
//  \/
//Does every character return 
//true for Character.isWhitespace?
```

Note:
Pre Java 11 has some issues though, because of .trim() handling of whitespace

+++

`Stream lines()`


`Returns a stream of lines extracted from this string, separated by line terminators.`

+++

```java
//Pre-Java 11
foobar.split("\\r?\\n"); //Array of Strings
//Java 11
foobar.lines(); //Stream of Strings
```

Note:
This method provides better performance than split("\R") by supplying elements lazily 
and by faster search of new line terminators.

+++

`String repeat(int)`


`Returns a string whose value is the concatenation of this string repeated count times.`

+++

```java
//Pre-Java 11
new String(new char[1337]).replace("\0", foobar);
//Java 11
foobar.repeat(1337);
```

Note:
Pre java 11 version from: https://stackoverflow.com/questions/1235179/simple-way-to-repeat-a-string-in-java
You create char of required lenght, all chars are predefined as \0 -> NUL, which we then replace to required string

+++

<ul style="font-size: 2rem;">
<li><code>String strip(): Returns a string whose value is this string, with all leading and trailing whitespace removed.</code></li>
<li><code>String stripLeading(): Returns a string whose value is this string, with all leading whitespace removed.</code></li>
<li><code>String stripTrailing(): Returns a string whose value is this string, with all trailing whitespace removed.</code></li>
</ul>

+++

```java
//Pre-Java 11
//Everything UP TO `U+0020` is trimmed
foobar.trim()
//Java 11
//Returns a string with all 
//Character.isWhitespace characters removed
foobar.stripLeading();
foobar.stripTrailing();
foobar.strip(); //foobar.stripLeading() +
				//foobar.stripTrailing()
```

Note:
Yep, BACKSPACE (U+0008) is whitespace and so is BELL (U+0007) but LINE SEPARATOR (U+2028) isn’t.
