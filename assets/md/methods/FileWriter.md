##### java.io.FileReader

`Two new constructors that allow a Charset to be specified.`

##### java.io.FileWriter

`Four new constructors that allow a Charset to be specified.`

+++

```java
//Pre-Java 11
//FileReader/Writer used default encoding
//Had to do for ex. this to set charset:
new InputStreamReader(new FileInputStream(file), charset)
```

+++

```java
//Java 11
//New constuctors with Charset
public FileReader(String fileName, Charset charset)
public FileReader(File file, Charset charset)

public FileWriter(String fileName, Charset charset)
public FileWriter(String fileName, Charset charset,
				  boolean append)
public FileWriter(File file, Charset charset)
public FileWriter(File file, Charset charset,
				  boolean append)
```
