##### java.util.function.Predicate

 `Predicate not(Predicate)` 


 `Returns a predicate that is the negation of the supplied predicate.`

+++

```java
var emptyList = Collections.singletonList("");

//Pre-Java 11
emptyList.stream()
        .filter(it -> !it.endsWith("jug"));

//Java 11
emptyList.stream()
        .filter(Predicate.not(it -> it.endsWith("jug")));
//or with static import
emptyList.stream()
        .filter(not(it -> it.endsWith("jug")));
```
