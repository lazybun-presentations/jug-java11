### Summary

* No revolutions
* Mostly quality of life improvements
* My top 3:
  * `Predicate.not`
  * `Optional.isEmpty`
  * `String` stuff

---

### Sources

* Summaries of Java 11 changes
  * https://dzone.com/articles/90-new-features-and-apis-in-jdk-11 
  * http://blog.codefx.org/java/java-11-gems/
* A diff between JDK10 and JDK11
  * https://gunnarmorling.github.io/jdk-api-diff/jdk10-jdk11-api-diff.html

Note:
Some changes were skipped. Refer to these sources for more information
  
---

### Thank you!

@fa[twitter gp-contact]() @bulaleniwa

@fa[github gp-contact]() lazybun

@fa[gitlab gp-contact]() lazybun

@fa[envelope gp-contact]() krzysztof@piszkod.pl