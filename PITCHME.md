---?include=assets/md/introduction.md

---?include=assets/md/overview.md

---

## Let's get to those new methods

Note:
Sources - 
https://dzone.com/articles/90-new-features-and-apis-in-jdk-11
http://blog.codefx.org/java/java-11-gems/

---?include=assets/md/methods/ByteArrayOutputStream.md

---?include=assets/md/methods/InputStream.md

---?include=assets/md/methods/TimeUnit.md

---?include=assets/md/methods/Path.md

---?include=assets/md/methods/CharSequence.md

---?include=assets/md/methods/FileWriter.md

---?include=assets/md/methods/Files.md

---?include=assets/md/methods/PriorityQueue.md

---?include=assets/md/methods/Collection.md

---?include=assets/md/methods/Optional.md

---?include=assets/md/methods/Predicate.md

---?include=assets/md/methods/Pattern.md

---?include=assets/md/methods/String.md

---?include=assets/md/end.md