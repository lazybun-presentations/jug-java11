Cześć. Witam na mojej prezentacji Rewolucyjne zmiany w API Javy 11. 

Na początek krótko o mnie - nazywam się Krzysiek Piszko, profesjonalnie pracuję jako programista od już ponad 3 lat, choć samym programowaniem interesowałem i zajmowałem się już o wiele wcześniej. Aktualnie pracuje w małej firmie o bardzo prostej do wypowiedzenia nazwie NetworkedAssets.
No, ale to dość o mnie, bo contentu jest w sumie niemało.

No właśnie, ale ile dokładnie?

W sumie zmienionych zostało 946 klas. I w tym...

93 nowych. Trochę z nich to wynik JEP 321, kilka klas z security, sporo klas związanych z integracją Flight Recordera, o czym mogliśmy usłyszeć ostatnio na JUGu oraz trochę klas różnych.

Usuniętych zostało 787 klas. Głównie ze względu na JEP 320, ale również przesunięcie JEP 321 z inkubatora do core i z jakiegoś powodu su.reflect.Reflection

No i zmodyfikowano 66 klas z najróżniejszych paczek. Nie będę ich wymieniał bo po co ... 

przecież teraz o co ciekawszych powiem. Począwszy od tych - moim zdaniem - mniej ekscytujących zmian

---

Do Byte Array Output Stream dodano metodę, która zapisze dany byte array do Byte Array Output Streama. Taka metoda już istniała, ale w nieco innej formie.

Wcześniej musieliśmy podawać start i długośc arraya. Teraz robi on to sobie sam.

---

Jeśli kiedkolwiek potrzebowaliśmy streama który nic nie czyta, albo taki który wszystko wyrzuca, to teraz są one wbudowane.

Wcześniej musieliśmy je pisać sami

Ale teraz jak nam takowego do szczęścia potrzeba, to cyk jedna linijka i już.

---

Kolejna rewolucja dotknęła TimeUnit. Doczekała się ona metody convert, która zamieni dane Duration na liczbę żądanych jednostek.

<Przykład>

---

Path dostało statyczne metody `of`. Działają one praktycznie tak samo jak `Paths.get` 

---

W Character możemy teraz zmieniać unicodowy code point (code point to np U+0420) bezpośrednio na stringa zamiast zamieniać go najpierw na char array

---

Dalej mamy statyczną metodę do CharSequence, za pomocą której możemy porównywać dwa char sequence.

Przykład - wcześniej żeby porównać stringa do tego co mamy w string builderze, musieliśmy buildera toStringować. Teraz możemy podać bezpośrednio buildera.

---

File Reader i File Writer doczekały się nowych konstruktorów, za pomocą których możemy wymuszać charset.

Wcześniej musieliśmy robić taki piękny łańcuszek, bo FileReader i Writer brały domyślny charset. Teraz możemy podawać charset w kostruktorze.

---

A skoro o plikach mowa, podobnych metod doczekaliśmy się w Files.
Możemy teraz za pomocą pojedynczych metod czytać i zapisywać do pliku stringi z żądanych charsetem - domyślnie UTF-8

---

Do PriorityQueue i BlockingQueue dodali kilka metod.

Najlepiej na nie rzucić okiem na przykładach...

---

Ciekawą metodę dodano do kolekcji. Samo toArray, rozumie się zamo przez się, ale spójżmy na przykład

Wcześniejsze sposoby tworzenia albo były krótkie, lecz nie zachowywały typu, albo były dłuższe i "brzydkie". Teraz możemy podać jako parametr method reference na żądany konstruktor i już. Według mnie trochę czytelniejsze.

---

Teraz możemy tworzyć alternatywny predykat z patterna.

Dotychczas, mogliśmy zrobić `asPredicate`, które matchowało jakikolwiek substring. Teraz, za pomocą asMatchPredicate, możemy wymusić matchowanie albo całego stringa albo niczego.

---

Kolejnym poprawiającym czytelność kodu dodatkiem jest `isEmpty` dla optionali. 

Wcześniej musieliśmy negować `isPresent`, teraz wystarczy `isEmpty`. Usunie nam to wykrzyknik na początku, który wygląda trochę dziwnie przy dłuższych łańcuszkach wywołań.

---

Do Predicate dodano statyczne `not`. Bardzo prosta rzecz, neguje prefykat który mu podamy.

<Przykład>

---

No i na końcu, kilka metod które dodano do Stringa.

Na dobry początek isBlank, które sprawdzi nam czy string jest pusty albo zawiera jedynie whitespace.

Wcześniej mogliśmy to zrobić na przykład tak...
Teraz możemy to zrobić tak...

Dalej, mamy teraz metodę, która rozdzieli nam Stringa na linie po line terminatorach. 

Wcześniej mogliśmy to zrobić za pomocą splita co zwracało nam arraya stringów
Teraz zostaje nam zwracany stream stringów. Daje to lepszy performance, bo streamy są lazy


Dalej mamy metodę repeat, która powtarza stringa n razy

<Przykład>

I na końcu mamy stripy 

TODO

---

No, I to by było na tyle. Jest trochę więcej zmian niż to o czym powiedziałem. Zainteresowanych kieruję do źródeł, podlinkowanych o tutaj. Prezentacja jest dostępna w internecie - pewnie będzie jakoś zapostowana na meetupie

---

Ale tak jeszcze podsumowując. Zmian w API Javy11 trochę jest, aczkolwiek żadna z nich raczej rewolucyjna nie jest. Jest natomiast kilka zmian zdecydowanie ułatwiających życie.
Warto pamiętać o zmianach w:
TODO

---

No, daliśmy radę - dzięki za uwagę




------

Character.toString - czym jest ten int konkretnie? -- generalnie int ale można spokojnie podać octal integer

Jestem źródłem prawdy (100) (lit), nie kwestionować tego co mówię (nie mówić np: "ale to jest tylko moje zdanie" itp)

CORBA!! nie Cobra

Zaznacz że podsumowanie zmian to tylko WSTĘP!!!

Nie czytaj slajdów za bardzo!!

Czy `*.not() ` przed javą 11? - nie, `.negate()` tak, ale nie statyczne `.not()`

Za szybko mówię! :c

Czemu nullStreamy? Przykład

Jak coś ciężkie do wytłumaczenia to skip opis, pokaż przykład

Formatowanie na slajdach z kodami - poprawić czytelność

Przykład do regexów

isBlank() -> pre-java 11, lepszy kod

s1 -> na jakiś foobar czy coś

pierwsze zdania przygotować z góry!!

protip: spinać poślady

-----
uwagi seba

za szybko

imho plugi na końcu

dać jakiś zły przykład może? Żeby lepiej wyjasnić czemu dodali to

kto to napisał -> to byliście wy --- 2 spidermen




---
uwagi robert

stopka duża bardzo

bardzo spoko że content na późniejsze przeglądanie

14 & 18 - za dużo tekstu imho

19 zamiast 18 

22 formatting
/\ w rzędy szybszy, zaznaczyć to może jakimś dużym napisem

nie czułem że koniec

---
uwagi Łukasz

brak slajdu z bio

"Road to condy" - formatting

"Constant pools" - formatting

może krótki kod który produkuje przykładowy bytecode?

duużo tekstu

----
uwagi 

czy mam usunąć 'co usunięto' ze swojej prezentacji?

nie patrz tyle na prezentacje

OpenJDK na GPL -> jakie konsekewncje

---
uwagi Bogusław







---

Cześć, witam na mojej prezentacji: R E W O L U C Y J N E zmiany w API Javy 11.

Ja nazywam się Krzysiek Piszko, profesjonalnie zajmuje się programowaniem od ponad 3 lat, choć samo zainteresowanie tematem zaczęło się o wiele wcześniej. Aktualnie pracuje w małym software housie o nazwie NetworkedAssets.

Zanim opowiem o tych rewolucyjnych zmianach, trochę ogólnych informacji o tym ile właściwie było zmian.

Ogółem, poprzez dodanie, usunięcie bądź modyfikacje zmieniono 946 klas.

Z czego 93 klasy zostały dodane. Głownie poprzez wprowadzenie standardowego klienta http oraz Flight Recordera.

Usunięte zostało 787 klas. W tym releasie twórcy javy wzięli na siebie przedsięwzięcie usunięcia klas związanych z javą enterprise edition oraz modułem CORBA - stąd większość tych usunięć.

No i to na czym skupimy się w dzisiejszej prezentacji, czyli 66 klas zmodyfikowanych.

No to co, zaczynajmy!

